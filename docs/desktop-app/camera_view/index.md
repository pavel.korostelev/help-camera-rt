# Экран камеры

На экране камеры находится ее плеер, в котором показывается видео с камеры.

![Экран с плеером камеры](./images/screen_with_camera_player.jpg)

Буква «Д» на экране обозначает, что для снижения нагрузки воспроизводится дополнительный поток (сабпоток) с камеры.

В режимах приложения «Облако» и «Сеть» доступен переход на экран любой камеры, а в режиме «Только сеть» можно открыть только локальные камеры (подробнее в разделе «Ограничения функций в режиме офлайн»).

## Управление просмотром камеры

Под плеером расположен таймлайн для управления просмотром.

![Таймлайн камеры](./images/timeline_cameras.jpg)

Чтобы перейти к просмотру архивного видео от камеры, нажмите на нужное время на таймлайне или укажите дату и время, видео за которые нужно просмотреть, нажав на кнопку «Сегодня» и вызвав календарь.

Для пересмотра к прямому эфиру нажмите на кнопку «LIVE».

На таймлайне находится кнопки для управления показом видео:

- пауза/запуск показа;
- перемотка.

Кнопки с плюсом и минусом слева внизу таймлайна служат для изменения его масштаба.

Рядом с таймлайном (справа от него) расположены кнопки для:

1. Перехода в широкоэкранный режим. Также развернуть камеру на весь экран и свернуть обратно можно двойным кликом.
2. Ускорения и замедления воспроизведения на выбранный коэффициент. Чтобы ускорить или замедлить показ видео, нажмите на кнопку и выберите нужный коэффициент в выпадающем списке.

Как скрыть таймлайн в полноэкранном режиме, описано в разделе «Настройки».

Просмотр камеры и управление им доступны для тех камер, видео от которых поступает в приложение в настоящий момент. В режиме приложения «Облако» эти функции доступны для камер, видео от которых поступает в приложение с серверов системы. В режиме приложения «Сеть» эти функции доступны только для тех камер, которые находятся в одной локальной сети с ПК.

## Просмотр информации о камере

Чтобы вызвать окно с информацией о камере, нажмите кнопку с буквой «i» внизу плеера. Появится выпадающее окно, в котором указаны характеристики камеры и установленного на ней программного обеспечения.

![Окно с информацией о камере](./images/camera_information_window.jpg)

Из окна с информацией о камере можно перейти на сайт camera.rt.ru.

## Переход к странице камеры на сайте

В приложении нельзя изменять настройки камеры, но вы можете перейти на сайт camera.rt.ru и поменять настройки камеры там. Для этого на ПК должен быть установлен браузер.

1. Нажмите кнопку с буквой «i» в плеере камеры.
2. Нажмите «Открыть Web».

Приложение запустит браузер на ПК, чтобы открыть сайт, или воспользуется уже запущенным. Если вы не авторизованы на сайте в этом браузере, откроется страница авторизации. Если вы авторизованы, то откроется страница камеры в вашем личном кабинете.

Переход к странице камеры на сайте доступен только в режиме «Облако» и при наличии подключения к Интернету.