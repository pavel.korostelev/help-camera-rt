# Установка

Для установки скачайте и запустите на ПК файл с установщиком приложения.

<iframe width="640" height="360" src="https://www.youtube.com/embed/cvDQxwkWxGo?list=PLVj1zL_0C3SDWhWWlztIatK-YNPpipQav" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![Начало установки](./images/setup-welcome-screen.png)

Укажите папку, в которую нужно установить приложение.

![Выбор директории](./images/setup-select-folder.png)

Подтвердите установку.

![Подготовка к установке](./images/setup-confirm-install.png)

Процесс установки происходит автоматически и может занять несколько минут.

![Процесс установки](./images/setup-processing.png)

![Установка завершена](./images/setup-completed.png)

После завершения установки в выбранной вами директории окажется исполняемый файл приложения. 
Также на рабочем столе и в меню Пуск появятся ярлыки для запуска приложения.