# Авторизация

1. Запустите приложение.
2. Введите учетные данные аккаунта (почту/логин и пароль). Нажимая на глаз в поле ввода пароля, вы делаете пароль видимым.
3. Нажмите «Войти».

Можно поставить галочку напротив «Запомнить пароль и не спрашивать в дальнейшем». Тогда при следующем запуске приложение сразу перейдет к главному окну, пропуская авторизацию.

![Форма авторизации](./images/authentication.png)

## Возможные проблемы при авторизации

Приложение сообщит вам, если:

- у ПК нет доступа в Интернет;
- вы ввели неверные учетные данные;
- ваша организация заблокирована в системе.

Если у приложения нет доступа в Интернет, восстановите связь с Интернетом. Вы всё ещё можете авторизоваться по логину и паролю пользователя, последним выполнившего вход в онлайн-режиме (подробнее в разделе «Ограничения функций в режиме офлайн»).

Если ваша организация заблокирована, обратитесь к менеджеру Ростелекома.