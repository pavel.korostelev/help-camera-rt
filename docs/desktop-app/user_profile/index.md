# Профиль

Чтобы посмотреть информацию в профиле своего аккаунта в приложении, нажмите на блок с вашей почтой и названием организации, находящийся в правом верхнем углу. В открывшемся окне будет указаны настройки профиля и количество доступных камер.

![Окно профиля пользователя](./images/user_profile_window.png)

Просмотр профиля доступен в обоих режимах приложения («Облако» и «Сеть»).