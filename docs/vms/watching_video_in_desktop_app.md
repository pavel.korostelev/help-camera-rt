# Просмотр видео от тепловизора в Настольном приложении

В разделе указано, как просматривать видео от тепловизора в онлайн- и оффлайн-режиме.

## Подготовка

1. Должно быть установлено Настольное приложение.
2. Должен быть установлен VMS-сервер, к нему должен быть подключен тепловизор.
3. Нужно знать логин и пароль от VMS-сервера — те, по которым происходит авторизация в интерфейсе VMS. 

## Онлайн-режим

1. Откройте Настольное приложение «Видеонаблюдение v2 (x64)», которое вы установили на шаге «Установка Настольного приложения», через рабочий стол или меню «Пуск». Авторизация не требуется.

    ![](img/Online_Installation_42.png)
    
2. Проверьте, что вы находитесь на вкладке «Камеры». В ней должен находиться VMS-сервер, который вы установили на этапе «Добавление VMS к Личному кабинету Ростелеком». 
    - Сине-зелёный цвет индикатора говорит о том, что устройство подключено к серверам Ростелекома и работает.
    - Красный индикатор говорит о том, что VMS-сервер не работает. Проверьте подключение к Интернету.

    ![](img/Online_Installation_43.png)
    
3. Нажмите на строку VMS-сервера. Откроется страница со списком подключенных к нему камер, включая тепловизор.

    ![](img/Online_Installation_44.png)
    
    Если тепловизора нет в списке камер, проверьте, что он подключен к VMS-серверу, через интерфейс самого VMS. Для этого откройте браузер и введите в адресную строку `http://127.0.0.1:8080/`. Проверьте, что в списке камер у вас есть тепловизор, который вы добавили в разделе «Добавление тепловизора к VMS».
4. Нажмите на строку тепловизора. Если все сделано правильно, на экране будет проигрываться видео от тепловизора.

    ![](img/Online_Installation_45.png)

## Оффлайн-режим

1. Откройте Настольное приложение «Видеонаблюдение v2 (x64) (ограниченный режим)» через рабочий стол или меню «Пуск». Авторизация не требуется.

    ![](img/Offline_Installation_36.png)

2. Проверьте, что вы находитесь на вкладке «Устройства». 
3. Нажмите на кнопку-плюс для добавления VMS-сервера.

    ![](img/Offline_Installation_37.png)

4. Введите:
    - IP-адрес VMS-сервера — 127.0.0.1
    - логин VMS-сервера, по умолчанию admin
    - пароль VMS-сервера, по умолчанию admin54321

    ![](img/Offline_Installation_38.png)

5. Нажмите «Добавить».
    - Если все прошло успешно, то VMS-сервер появится в списке устройств. То, что он работает, видно по синему цвету его индикатора.

    ![](img/Offline_Installation_39.png)

    - Если произошла ошибка, попробуйте добавить VMS-сервер снова. Проверьте, что VMS-сервер подключен к сети и что вы верно ввели его логин, пароль и IP-адрес.
6. Нажмите на строку VMS-сервера. Откроется страница со списком подключенных к нему камер, включая тепловизор.

    ![](img/Offline_Installation_40.png)

    - Если тепловизора нет в списке камер, проверьте, что он подключен к VMS-серверу, через интерфейс самого VMS. Для этого откройте браузер и введите в адресную строку http://127.0.0.1:8080/. Проверьте, что в списке камер у вас есть тепловизор, который вы добавили в разделе «Добавление тепловизора к VMS».
7. Нажмите на строку тепловизора. Если все сделано правильно, на экране будет проигрываться видео от тепловизора.

    ![](img/Offline_Installation_41.png)

## Включение записи видео (для оффлайн-режима)

Видеопоток от тепловизора нарезается на части, которые можно сохранить на устройстве. По умолчанию в offline-режиме запись видео на устройство выключена. Чтобы включить её, воспользуйтесь алгоритмом:

1. Через браузер зайдите на адрес: http://localhost:8080/swagger-ui.html#/vms/setStorageDeviceParams. Здесь содержится описание методов API, используемых в системе.
2. В разделе «vms» (API для VMS) найдите метод «PUT /storage_device/params: Установить параметры устройства хранения». Если вы заходили по ссылке, указанной выше, этот метод будет развёрнут, в отличие от остальных.
3. Нажмите на кнопку «Try it out» в правой верхней части окна метода. После этого вы можете редактировать метод.

    ![](img/Offline_Installation_42.png)

4. Сотрите содержимое метода и вставьте вместо него путь, в который предусмотрена запись видеопотока с тепловизора. Например:

    ```
    { 
    "storage_path": "/mnt/c/vms/"
    } 
    ```

5. Подтвердите действие, нажав кнопку «Execute» внизу окна метода.

    ![](img/Offline_Installation_43.png)

Аналогичным образом включите запись для каждой из камер:

1. Через браузер зайдите на адрес: http://localhost:8080/swagger-ui.html#/camera/getRecordingSchedule.
2. В разделе «camera» (API для камер) найдите метод «PUT /stream/recording/schedule: Получить расписание записи». Если вы заходили по ссылке, указанной выше, этот метод будет развёрнут, в отличие от остальных.
3. Нажмите на кнопку «Try it out» в правой верхней части окна метода. После этого вы можете редактировать метод.

    ![](img/Offline_Installation_44.png)

4. Введите количество камер в поле camera.
5. Отредактируйте содержимое метода:

    ```
    { 
    "epoch_intervals": [], 
    "local_intervals": [],
    "type": "DISABLES",
    "weekly_intervals": []
    } 
    ```
    
6. Подтвердите действие, нажав кнопку «Execute» внизу окна метода.

    ![](img/Offline_Installation_45.png)