param ([string]$sourceFolder, [string]$sourceExcludeList, [string]$destFolder, [string]$destExcludeList)

$sourceFolderFullPath = Resolve-Path $sourceFolder
$destFolderFullPath = Resolve-Path $destFolder

# Prepare selectors for Compare-Object that cuts absolute path to relative ones
$sourceFolderLength = $sourceFolderFullPath.ToString().Length
$sourceNameSelector = @{label="Name";expression={$_.FullName.ToString().Substring($sourceFolderLength)}}
$destFolderLength = $destFolderFullPath.ToString().Length
$destNameSelector = @{label="Name";expression={$_.FullName.ToString().Substring($destFolderLength)}}

try {
  # Get both folder contents with relative paths
  $currentFiles = @(Get-ChildItem -Exclude $destExcludeList -Path "$destFolderFullPath" -ErrorAction SilentlyContinue | Get-ChildItem -Recurse | Select-Object -Property $destNameSelector)
  $newFiles = @(Get-ChildItem -Exclude $sourceExcludeList -Path "$sourceFolderFullPath" -ErrorAction SilentlyContinue | Get-ChildItem -Recurse | Select-Object -Property $sourceNameSelector)
  
  # Compare both folder contents
  $comparison = Compare-Object -ReferenceObject $currentFiles -DifferenceObject $newFiles -Property Name
  
  # Remove files from destination folder that should be removed
  $comparison | Where-Object { $_.SideIndicator -eq "<=" } | foreach { $fsoName = "$destFolderFullPath$($_.Name)"; Write-Output "Trying to delete $fsoName"; Remove-Item -Force -Recurse -Path $fsoName }
  
  # Copying all other items
  # ToDo: co not copy files, that is not changed
  Copy-Item -Force -Recurse "$sourceFolderFullPath\*" -Destination "$destFolderFullPath"
}
catch {
  Write-Output $PSItem.ToString()
}